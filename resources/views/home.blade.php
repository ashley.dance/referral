@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">

                    <h3>Current Codes</h3>
    
                    @if( !$codes->isEmpty() )

                        @foreach( $codes as $code )
                            <h3>code example</h3>
                        @endforeach

                    @else
                        <h3>Currently no codes, add some now</h3>
                    @endif

                    

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
